---
title: "A Self-Generalizing Coin Change Problem"
date: 2021-03-14T12:26:24+01:00
draft: true
math: true
---

# The Problem
A good friend of mine recently came to me with a problem that had stumped him. 
It was presented as a coding exercise alongside the standard dynamic-programming exercise associated with changing coins. You know the one, find the minimum number of coins needed to provide change given a set of coin denominations and an amount.
The wording of this second exercise was something along the lines of

> **Assume you have a set of coin denominations \\( \mathbf \{ \mathcal C = \\{ c\_1, \ldots, c\_n \\} \} \\), all of which are positive integers, and that you are only allowed to provide change by handing out at most one coin of each denomination. Find the first _unreachable_ amount, i.e. the first amount for which it is not possible to provide change when you can use at most one coin of each denomination.**

Of course, it's possible to attack this through sheer brute force: Generate all the possible subset sums of \\( \mathcal C \\), sort the list, and find the first gap. But generating all the subset sums is clearly an \\( O(2^n) \\) operation, and it should surely be possible to do better than exponential time, surely?

# The Approach
Let's establish some initial terminology. A denomination is simply a positive integers. Assume we're given a set \\( \mathcal C \\) as above. Let's call an amount _reachable_ if it is possible to express it as a sum of at most one coin of each denomination.

Call a set \\( \\{ c\_1, \ldots, c\_k \\} \\) of denominations _full_ if every integer from 1 to \\( c\_1 + \cdots + c\_k \\) can be found as a subset sum. 

**Lemma:** 
\\[ U(\mathcal C) - c\_i = c\_i + r\_i \\]
so that
\\[ U(\mathcal C) = r + \sum\_{i=1}^k c\_k \\]


**Lemma:** \\(U(\mathcal C)\\) is of the form \\(1 + \sum\_{i=1}^k c\_i \\) for some \\(k\\), \\( \mathcal C\_k \\) is full and \\( \mathcal C\_{k+1} \\) is non-full.

# The Result

$$ U-1 \leq \sigma(c\_1, \ldots, c\_k) < U $$




Let \\( k \\) be the first integer for which $$ 1 + \sum\_{i=1}^{k-1} c\_i < c\_{k} $$
then the first unreachable amount is $$ U(\mathcal C) = 1 + \sum\_{i=1}^{k-1} c\_i $$

This can be shown inductively. Let \\( \mathcal C\_0 = \emptyset \\), and let's say that 

That gives us the following unreasonably simple O(n) algorithm
``` python
def first_unreachable(coins):
    unreachable = 1
    for c in coins:
        if c > unreachable:
            break
        unreachable += c

    return unreachable
```

# Recapitulation
This is an interesting result, since it has an analogy to binary numbers. If the denominations were simply \\( c\_i = 2^{i-1} \\), the question becomes identical to "what is the first integer not representable with _n_ bits", which is clearly \\( 2^n = 1 + \sum 2^i \\).

# The Generalization 

